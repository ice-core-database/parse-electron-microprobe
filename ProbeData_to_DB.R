library(tidyverse)
library(DBI)
library(dplyr)
library(stringr)
library(dotenv)
#load_dot_env(file = ".env")
load_dot_env(file = "dev.env")

# Here are the dictionaries for lab, instrument, and core. Set the values as appropriate for the data being imported.
source('metadata_dictionaries.R')
source('parse_electron_microprobe.R')

## Opening the connection to the database
con <- con <- dbConnect(RPostgres::Postgres(), dbname = Sys.getenv("DATABASE_NAME"), host = Sys.getenv("DATABASE_HOST"),
                        port = Sys.getenv("DATABASE_PORT"), user = Sys.getenv("DATABASE_USER"),
                        password = Sys.getenv("DATABASE_PASS"))

probe <- parse_probe_csv(probe_data_path = '~/Denali/North_Pacific_Data/Denali/DIC2/Volcanics/Microprobe/HannaDenali/Probe Data_Oct2022.csv',
                         con = con,
                         sample_core_id = core_id_dictionary["DEN-13B"],
                         probe_lab_id = lab_id_dictionary["Electron Microprobe Laboratory"],
                         probe_instrument_id = instrument_id_dictionary["SX-100"])

# Prepare the sample table for upload
sample <- probe %>% select(tube_id, sample_id, sample_name, core_id)
# Upload sample to the database :)
dbAppendTable(con, Id(schema = "denali", table = "sample"), sample)

# Make probe Table and clean up column names
probe_data <- probe %>% get_probe_table(con)
# Upload the probe data to the database
dbAppendTable(con, Id(schema = "denali", table = "normalized_probe"), probe_data)

## disconnect from database
dbDisconnect(con)